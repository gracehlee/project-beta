# Generated by Django 4.0.3 on 2024-03-19 20:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0005_alter_customer_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='id',
            field=models.IntegerField(default=1, primary_key=True, serialize=False, unique=True),
        ),
    ]
