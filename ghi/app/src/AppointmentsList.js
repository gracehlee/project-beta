import React, { useEffect, useState } from "react";


function AppointmentsList() {
    const [appointments, setAppointments] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8080/api/appointments/";
        const response = await fetch(url);
        const automobileUrl = "http://localhost:8080/api/automobilevos/";
        const voResponse = await fetch(automobileUrl);

        if (response.ok && voResponse.ok) {
            const data = await response.json();
            const vo = await voResponse.json();

            const appointmentList = data.appointments;
            const voList = vo.automobilevos;
            const vinList = voList.map(vo => vo.vin)

            // add VIP info and format date_time object
            for (let appointment of appointmentList) {
                if (vinList.includes(appointment.vin)) {
                    appointment["vip"] = "Yes";
                } else {
                    appointment["vip"] = "No";
                }
                let date = new Date(appointment.date_time)
                appointment["date"] = date.toLocaleDateString('en-US');
                appointment["time"] = date.toLocaleTimeString('en-US', {
                    hour: 'numeric',
                    minute: 'numeric',
                    second: 'numeric',
                    hour12: true,
                });
            }
            setAppointments(appointmentList);
        };
    };

    useEffect(() => {
        fetchData();
    }, []);

    const cancelAppointment = async (appointmentId) => {
        const url = `http://localhost:8080/api/appointments/${appointmentId}/cancel/`;
        await fetch(url, { method: "put" });
        fetchData();
    }

    const finishAppointment = async (appointmentId) => {
        const url = `http://localhost:8080/api/appointments/${appointmentId}/finish/`;
        await fetch(url, { method: "put" });
        fetchData();
    }


    return (
        <>
            <br></br><h1>Appointments</h1><br></br>
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>VIN</th>
                  <th>Is VIP?</th>
                  <th>Customer</th>
                  <th>Date</th>
                  <th>Time</th>
                  <th>Technician</th>
                  <th>Reason</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                {appointments?.filter(appointment => appointment.status === "created").map(appointment => {
                  const {id, vin, vip, customer, date_time, technician, reason } = appointment;
                  const date = new Date(date_time);
                  const timeFormat = {
                    hour: 'numeric',
                    minute: 'numeric',
                    second: 'numeric',
                    hour12: true,
                  }
                  return (
                    <tr key={id}>
                      <td>{ vin }</td>
                      <td>{ vip }</td>
                      <td>{ customer }</td>
                      <td>{ date.toLocaleDateString('en-US') }</td>
                      <td>{ date.toLocaleTimeString('en-US', timeFormat)}</td>
                      <td>{ technician.first_name } { technician.last_name }</td>
                      <td>{ reason }</td>
                      <td>
                        <button className="btn btn-danger" onClick={() => cancelAppointment(appointment.id)}>Cancel</button>
                        <button className="btn btn-primary" onClick={() => finishAppointment(appointment.id)}>Finish</button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
          </table>
        </>
      );
};

export default AppointmentsList
