import React, { useEffect, useState } from "react";


function SalespersonHistory() {
  const [sales, setSales] = useState([]);
  const [salespeople, setSalespeople] = useState([]);
  const [salesperson, setSalesperson] = useState("");

  const handleChangeSalesperson = (event) => {
    const value = event.target.value;
    setSalesperson(value);
  }

  const getSales = async function() {
    const url = 'http://localhost:8090/api/sales/'
    const response = await fetch(url)
    if (response.ok) {
      const sales = await response.json();
      setSales(sales);
    }
  }

  const getSalespeople = async function() {
    const url = 'http://localhost:8090/api/salespeople/'
    const response = await fetch(url)
    if (response.ok) {
      const salespeople = await response.json();
      setSalespeople(salespeople)
    }
  }

  useEffect(() => {
    getSales();
    getSalespeople();
  }, []);

  return (
    <>
      <br></br><h1>SalesPerson History</h1><br></br>
      <div className="form-floating mb-3">
        <select onChange={handleChangeSalesperson} name="salesperson" id="salesperson" className="form-select" value={salesperson}>
          <option value={''}>Choose a Salesperson</option>
          {salespeople?.map(person => {
            return (
              <option key={person.employee_id} value={person.employee_id}>{person.first_name} {person.last_name}</option>
            );
          })};
        </select>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales?.filter((sale) => parseInt(sale.salesperson.employee_id) === parseInt(salesperson)).map(sale => {
            return (
              <tr key={sale.id}>
                <td>{ sale.salesperson.first_name } {sale.salesperson.last_name}</td>
                <td>{ sale.customer.first_name } {sale.customer.last_name}</td>
                <td>{ sale.automobile.vin }</td>
                <td>${sale.price}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  )
}

export default SalespersonHistory
