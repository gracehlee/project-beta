import React, { useState } from "react";


function CustomerForm() {
    const [formData, setFormData] = useState({
        firstName: "",
        lastName: "",
        address: "",
        phoneNumber:"",
    });

    const handleChange = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value,
        })
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.first_name = formData.firstName;
        data.last_name = formData.lastName;
        data.address = formData.address;
        data.phone_number = formData.phoneNumber;

        const customerUrl = "http://localhost:8090/api/customers/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
            await response.json();
            setFormData({
                firstName: "",
                lastName: "",
                address: "",
                phoneNumber:"",
            });
        };
    };

    const { firstName, lastName, address, phoneNumber} = formData;

    return (
        <>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1 style={{display: 'grid', justifyContent: 'center'}}>Add a Customer</h1>
                        <form onSubmit={handleSubmit} id="add-customer-form" >
                            <div className="form-floating mb-3">
                                <input onChange={handleChange} value={firstName} placeholder="First Name" required type="text" name="firstName" id="firstName" className="form-control"/>
                                <label htmlFor="firstName">First Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleChange} value={lastName} placeholder="Last Name" required type="text" name="lastName" id="lastName" className="form-control"/>
                                <label htmlFor="lastName">Last Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleChange} value={phoneNumber} placeholder="Phone Number" required type="number" name="phoneNumber" id="phoneNumber" className="form-control"/>
                                <label htmlFor="phoneNumber">Phone Number</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleChange} value={address} placeholder="Address" required type="text" name="address" id="address" className="form-control"/>
                                <label htmlFor="address">Address</label>
                            </div>
                            <div style={{display: 'grid', justifyContent: 'center'}}>
                                <button className="btn btn-primary">Create</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </>
    );
}

export default CustomerForm
