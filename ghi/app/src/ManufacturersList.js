import React, { useEffect, useState } from "react";


function ManufacturersList() {
    const [manufacturers, setManufacturers] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
            <br></br><h1>Manufacturers</h1><br></br>
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Manufacturer Name</th>
                </tr>
              </thead>
              <tbody>
                {manufacturers?.map(manufacturer => {
                  return (
                    <tr key={manufacturer.id}>
                      <td>{ manufacturer.id }</td>
                      <td>{ manufacturer.name }</td>
                    </tr>
                  );
                })}
              </tbody>
          </table>
        </>
      )
}

export default ManufacturersList
