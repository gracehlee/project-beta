"""sales_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path
from sales_rest.views import (
    api_automobilevo,
    api_automobilevo_detail,
    api_list_salespeople,
    api_salespeople_detail,
    api_list_customers,
    api_detail_customer,
    api_list_sales,
    api_detail_sale,
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('automobilevo/', api_automobilevo, name="api_automobilevo"),
    path('automobilevo/<str:vin>/', api_automobilevo_detail, name="api_automobilevo_detail"),
    path('api/salespeople/', api_list_salespeople, name="api_list_salespeople"),
    path('api/salespeople/<int:employee_id>/', api_salespeople_detail, name="api_salespeople_detail"),
    path('api/customers/', api_list_customers, name="api_list_customers"),
    path('api/customers/<int:id>/', api_detail_customer, name="api_detail_customer"),
    path('api/sales/', api_list_sales, name="api_list_sales"),
    path('api/sales/<int:id>/', api_detail_sale, name="api_detail_sale"),
]
