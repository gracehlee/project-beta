import React, { useEffect, useState } from "react";


function SalespeopleList() {
    const [salespeople, setSalespeople] = useState([]);

    const getSalespeople = async function() {
      const url = 'http://localhost:8090/api/salespeople/'
      const response = await fetch(url)
      if (response.ok) {
        const salespeople = await response.json();
        setSalespeople(salespeople)
      }
    }

    useEffect(() => {
        getSalespeople();
    }, []);

    return (
        <>
            <br></br><h1>Salespeople</h1><br></br>
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>Employee Id</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                </tr>
              </thead>
              <tbody>
                {salespeople?.map(salesperson => {
                  return (
                    <tr key={salesperson.employee_id}>
                      <td>{ salesperson.employee_id }</td>
                      <td>{ salesperson.first_name }</td>
                      <td>{ salesperson.last_name }</td>
                    </tr>
                  );
                })}
              </tbody>
          </table>
        </>
      )
}

export default SalespeopleList
