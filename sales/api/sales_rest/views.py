import json
from django.http import JsonResponse

from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import AutomobileVo, Salesperson, Sale, Customer
# Create your views here.
from common.json import ModelEncoder




class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
        "id"
    ]
class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVo
    properties = [
        "vin",
        "sold",
    ]

class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id"
    ]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "price",
        "automobile",
        "customer",
        "salesperson",

    ]
    encoders= {"automobile":AutomobileVOEncoder(), "customer":CustomerEncoder(), "salesperson":SalespersonEncoder()}




@require_http_methods(["GET", "POST"])
def api_automobilevo(request):
    if request.method == "GET":
        autos = AutomobileVo.objects.all()
        return JsonResponse(
            {"autos": autos},
            encoder=AutomobileVOEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            auto = AutomobileVo.objects.create(**content)
            return JsonResponse(
                auto,
                encoder=AutomobileVOEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the automobile"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_automobilevo_detail(request, vin):
    if request.method == "GET":
        try:
            auto = AutomobileVo.objects.get(vin=vin)
            return JsonResponse(
                auto,
                encoder=AutomobileVOEncoder,
                safe=False
            )
        except AutomobileVo.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            auto = AutomobileVo.objects.get(vin=vin)
            auto.delete()
            return JsonResponse(
                auto,
                encoder=AutomobileVOEncoder,
                safe=False,
            )
        except AutomobileVo.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            auto = AutomobileVo.objects.get(vin=vin)

            props = ["sold"]
            for prop in props:
                if prop in content:
                    setattr(auto, prop, content[prop])
            auto.save()
            return JsonResponse(
                auto,
                encoder=AutomobileVOEncoder,
                safe=False,
            )
        except AutomobileVo.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
       salesperson =  Salesperson.objects.all()
       return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False
            )
    else:
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the employee, ensure you are using a unique employee id"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "DELETE"])
def api_salespeople_detail(request, employee_id):
    if request.method == "GET":
        salesperson = Salesperson.objects.get(employee_id=employee_id)
        return JsonResponse(
            salesperson, encoder=SalespersonEncoder, safe=False
        )
    else:
        try:
            salesperson = Salesperson.objects.get(employee_id=employee_id)
            salesperson.delete()
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})



@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse(
            customer, encoder=CustomerEncoder, safe=False
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create customer, ensure phone number is correct and unique"}
            )
            response.status_code = 400
            return response



@require_http_methods(["GET", "DELETE"])
def api_detail_customer(request, id):
    if request.method == "GET":
        customer = Customer.objects.get(id=id)
        return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
    else:
        try:
            customer = Customer.objects.get(id=id)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})





@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sale = Sale.objects.all()
        return JsonResponse(
            sale, encoder=SaleEncoder, safe=False
        )
    else:
        content = json.loads(request.body)
        try:

            automobile = AutomobileVo.objects.get(vin=content["automobile"])
            content["automobile"] = automobile
        except AutomobileVo.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Automobile"},
                status=400,
            )
        try:

            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Customer"},
                status=400,
            )
        try:

            salesperson = Salesperson.objects.get(employee_id=content["salesperson"])
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Salesperson"},
                status=400,
            )

        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale, encoder=SaleEncoder, safe=False
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_detail_sale(request, id):
    if request.method == "GET":
        sale = Sale.objects.get(id=id)
        return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
            )
    elif request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=id)
            sale.delete()
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            sale = Sale.objects.get(id=id)


            props = ["sold"]
            for prop in props:
                if prop in content:
                    setattr(sale, prop, content[prop])
            sale.save()
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Sale does not exist"})
            response.status_code = 404
            return response
