import React, { useEffect, useState } from "react";


function AppointmentForm() {
    const [formData, setFormData] = useState({
        vin: "",
        customer: "",
        date: "",
        time: "",
        technician: "",
        reason: "",
    });

    const handleChange = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value,
        })
    };

    const [technicians, setTechnicians] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8080/api/technicians/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.vin = formData.vin;
        data.customer = formData.customer;
        // combine the date and time
        let dateTime = new Date(formData.date + 'T' + formData.time);
        // set dateTime to iso format
        data.date_time = dateTime.toISOString();
        data.technician = formData.technician;
        data.reason = formData.reason;

        const manufacturerUrl = "http://localhost:8080/api/appointments/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(manufacturerUrl, fetchConfig);
        if (response.ok) {
            await response.json();
            setFormData({
                vin: "",
                customer: "",
                date: "",
                time: "",
                technician: "",
                reason: "",
            });
        };
    };

    const {vin, customer, date, time, technician, reason} = formData;

    return (
        <>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1 style={{display: 'grid', justifyContent: 'center'}}>Create a new service appointment</h1>
                        <form onSubmit={handleSubmit} id="create-appointment-form" >
                            <div className="form-floating mb-3">
                                <input onChange={handleChange} value={vin} placeholder="Automobile VIN" required type="text" name="vin" id="vin" className="form-control"/>
                                <label htmlFor="vin">Automobile VIN</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleChange} value={customer} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control"/>
                                <label htmlFor="customer">Customer</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleChange} value={date} placeholder="Date" required type="date" name="date" id="date" className="form-control"/>
                                <label htmlFor="date">Date</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleChange} value={time} placeholder="Time" required type="time" name="time" id="time" className="form-control"/>
                                <label htmlFor="time">Time</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={handleChange} value={technician} required id="technician" name="technician" className="form-select">
                                <option value="">Choose a Technician</option>
                                {technicians.map(technician => {
                                    return (
                                        <option value={technician.id} key={technician.id}>
                                            {technician.first_name} {technician.last_name}
                                        </option>
                                    );
                                })};
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleChange} value={reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control"/>
                                <label htmlFor="reason">Reason</label>
                            </div>
                            <div style={{display: 'grid', justifyContent: 'center'}}>
                                <button className="btn btn-primary">Create</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </>
    );
}

export default AppointmentForm
