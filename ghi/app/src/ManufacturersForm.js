import React, { useState } from "react";


function ManufacturersForm() {
    const [formData, setFormData] = useState({
        name: "",
    });

    const handleFormInput = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value,
        })
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(manufacturerUrl, fetchConfig);
        if (response.ok) {
            await response.json();
            setFormData({name: "",});
        };
    };

    return (
        <>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1 style={{display: 'grid', justifyContent: 'center'}}>Create a new manufacturer</h1>
                        <form onSubmit={handleSubmit} id="create-manufacturer-form" >
                            <div className="form-floating mb-3">
                                <input onChange={handleFormInput} value={formData.name} placeholder="Manufacturer Name" required type="text" name="name" id="name" className="form-control"/>
                                <label htmlFor="name">Manufacturer Name</label>
                            </div>
                            <div style={{display: 'grid', justifyContent: 'center'}}>
                                <button className="btn btn-primary">Create</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </>
    );
}

export default ManufacturersForm
