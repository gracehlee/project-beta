import React, { useState } from "react";


function SalespersonForm() {
    const [formData, setFormData] = useState({
        firstName: "",
        lastName: "",
        employeeId: "",
    });

    const handleChange = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value,
        })
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.first_name = formData.firstName;
        data.last_name = formData.lastName;
        data.employee_id = formData.employeeId;

        const salespersonUrl = "http://localhost:8090/api/salespeople/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(salespersonUrl, fetchConfig);
        if (response.ok) {
            await response.json();

            setFormData({
                firstName: "",
                lastName: "",
                employeeId: "",
            });
        };
    };

    return (
        <>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1 style={{display: 'grid', justifyContent: 'center'}}>Add a Salesperson</h1>
                        <form onSubmit={handleSubmit} id="add-technician-form" >
                            <div className="form-floating mb-3">
                                <input onChange={handleChange} value={formData.firstName} placeholder="First Name" required type="text" name="firstName" id="firstName" className="form-control"/>
                                <label htmlFor="firstName">First Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleChange} value={formData.lastName} placeholder="Last Name" required type="text" name="lastName" id="lastName" className="form-control"/>
                                <label htmlFor="lastName">Last Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleChange} value={formData.employeeId} placeholder="Employee Id" required type="number" name="employeeId" id="employeeId" className="form-control"/>
                                <label htmlFor="employeeId">Unique Employee Id</label>
                            </div>
                            <div style={{display: 'grid', justifyContent: 'center'}}>
                                <button className="btn btn-primary">Create</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </>
    );
}

export default SalespersonForm
